#!/usr/bin/env python
import sys, os

def main():
    #figure out our directories
    localpath = os.path.split(os.path.abspath(sys.argv[0]))[0]
    testdata = localpath
    testcode = os.path.join(localpath, 'code')
    if os.path.isdir(os.path.join(testdata, 'data')):
        DATADIR = testdata
    if os.path.isdir(testcode):
        CODEDIR = testcode

    #apply our directories and test environment
    os.chdir(DATADIR)
    sys.path.insert(0, CODEDIR)

    #run game and protect from exceptions
    try:
        import Main as main, pygame
        args = sys.argv.append(main.FULLSCREEN)
        main.main(sys.argv)
    except KeyboardInterrupt:
        print 'Keyboard Interrupt (Control-C)...'
        
if __name__ == '__main__':
    main()
    sys.exit()
