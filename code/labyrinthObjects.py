# Copyright 2015, 2016 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, random, time
random.seed()

import gameobjects

# General object stats
PLAYERSIZE = 32
MONSTERSIZE = 32
MISSILESIZE = 18
SPRITESIZE = 18
TILESIZE = 48
SPRITEOFFSET = (TILESIZE - SPRITESIZE) / 2
ENDLEVELDELAY = 2
CONTROLS1 = 0
CONTROLS2 = 1
CONTROLLERNUMS = (CONTROLS1, CONTROLS2)
BUTTON1 = 0
PLAYERSPEED = 3
MISSILESPEED = 8
PLAYERSPAWNS = 5

# Timers and action cycles
CYCLE1 = 12
CYCLE2 = 24
CYCLE3 = 36
CYCLE4 = 48
STANDARDCYCLE = CYCLE1
EXPLODINGCYCLE = CYCLE2
DYINGCYCLE = CYCLE4
WARPINGINCYCLE = CYCLE4
STARTCYCLE = 0
RESTARTCYCLE = 1
DYINGTIME = 2
INVULNERABLETIME = 9
ORBTIME = 9

# Directional constants
LEFT = 'left'
UP = 'up'
DOWN = 'down'
RIGHT = 'right'

# Wall tile index numbers
NORTH = 1 # 0001
EAST = 2 # 0010
SOUTH = 4 # 0100
WEST = 8 # 1000
OO = 38
NN = 2
NNN = 25
NE = 6
NNEE = 36
EE = 27
EEE = 10
SE = 32
SSEE = 17
SS = 45
SSS = 22
NS = 39
NNS = 24
SNS = 4
SW = 42
SSWW = 12
WW = 14
WWW = 37
NW = 31
NNWW = 19
WE = 33
EWE = 28
WWE = 30
SP1 = 19 # Spawn Point 1
SP2 = 24
SP3 = 30
SP4 = 40
# Odd walls (deprecated)
WSW = 44
SSW = 21
NNW = 35
WNW = 5
ESE = 43
SSE = 13
NNE = 20
ENE = 3

# List of tiles that have walls in each direction
WALLSDICT = {NORTH: (NN, NNN, NE, NNE, ENE, NNEE, NS, NNS, SNS, NW, NNW, WNW, NNWW),
             EAST:  (EE, EEE, NE, NNE, ENE, NNEE, WE, WWE, EWE, SE, SSE, ESE, SSEE),
             SOUTH: (SS, SSS, SE, SSE, ESE, SSEE, NS, NNS, SNS, SW, SSW, WSW, SSWW),
             WEST:  (WW, WWW, NW, NNW, WNW, NNWW, WE, WWE, EWE, SW, SSW, WSW, SSWW)}

# Sprite names
HEALERM = 'healer-m'
HEALERF = 'healer-f'
MAGEM = 'mage-m'
MAGEF = 'mage-f'
GHAST = 'ghast'
GHOUL = 'ghoul'
WIGHT = 'wight'
SHADE = 'shade'
GHOST = 'ghost'
DEATH = 'death'
PLAYERS = (HEALERM, HEALERF, MAGEM, MAGEF)
MONSTERS = (GHAST, WIGHT, SHADE)
#MONSTERS = (GHAST, GHOUL, SHADE, GHOST, DEATH)
HOLYMISSILE = 'holy-missile'
MAGICMISSILE = 'magic-missile'
DARKMISSILE = 'dark-missile'
MISSILES = (HOLYMISSILE, MAGICMISSILE, DARKMISSILE)
DIRCHANGEFREQ = 2
FIREFREQUENCY = 2

# Speed constants
SPEED1 = 1
SPEED2 = 2
SPEED3 = 3
SPEED4 = 5
SPEED5 = 6
MISSILESPEED = 8

# Sprite status
STARTING = 'starting'
ARRIVING = 'arriving'
INVULNERABLE = 'invulnerable'
STANDING = 'standing'
MOVING = 'moving'
FLYING = MOVING
CASTING = 'casting'
DYING = 'dying'
DEAD = 'dead'
ORBITING = 'orbiting'
DEPARTING = 'departing'
RESPAWNING = 'respawning'
NOTHING = 'nothing'
PLAYERCYCLES = {ARRIVING: CYCLE4, INVULNERABLE: CYCLE1, STANDING: CYCLE1, MOVING: CYCLE1, CASTING: CYCLE1, DYING: CYCLE4, DEPARTING: CYCLE4}
MONSTERCYCLES = {ARRIVING: CYCLE3, MOVING: CYCLE1, DYING: CYCLE3, ORBITING: CYCLE1, DEPARTING: CYCLE1}
MISSILECYCLES = {MOVING: CYCLE1, DYING: CYCLE2, DEPARTING: CYCLE1}
PLAYERSIZES = {ARRIVING: 32, INVULNERABLE: 32, STANDING: 18, MOVING: 18, CASTING: 18, DYING: 18, DEPARTING: 32}
MONSTERSIZES = {ARRIVING: 18, MOVING: 18, DYING: 32, ORBITING: 24, DEPARTING: 18}
MISSILESIZES = {MOVING: 18, DYING: 32, DEPARTING: 18}
# HACK: manually listing all sprite sizes since they are different from object sizes
PLAYERSIZES2 = {ARRIVING: 32, INVULNERABLE: 32, STANDING: 18, MOVING: 18, CASTING: 18, DYING: 18, DEPARTING: 32}
MONSTERSIZES2 = {ARRIVING: 32, MOVING: 18, DYING: 18, ORBITING: 24, DEPARTING: 18}
MISSILESIZES2 = {MOVING: 18, DYING: 32, DEPARTING: 18}

GODIRECTIONS = {LEFT: gameobjects.LEFT, DOWN: gameobjects.DOWN, UP: gameobjects.UP, RIGHT: gameobjects.RIGHT}
ACTIONS = {UP: 0, LEFT: 1, DOWN: 2, RIGHT: 3,
           DYING: 4, INVULNERABLE: 5}
OPPOSITE = {UP: DOWN, LEFT: RIGHT, DOWN: UP, RIGHT: LEFT}

# Coordinates for the off-battlefield position of each player
OUTPOSITIONS1 = []
OUTPOSITIONS2 = []
SPRITESIZE = PLAYERSIZES[ARRIVING]
for i in range(64, 144, 16):
    OUTPOSITIONS1.append((i, 24, SPRITESIZE, SPRITESIZE))
for i in range(328, 408, 16):
    OUTPOSITIONS2.append((i, 24, SPRITESIZE, SPRITESIZE))
OUTPOSITIONS = (OUTPOSITIONS1,
                OUTPOSITIONS2)

class Player(gameobjects.Shooter):
    def __init__(self, name, rect):
        self.respawns = PLAYERSPAWNS
        self.respawnStartTime = time.time()
        self.dyingStartTime = 0
        self.score = 0
        self.status = DYING
        self.step = 0
        self.action = 2
        super(Player, self).__init__(name, rect)
        self.missile.width = 16
        self.missile.height = 16
        self.missile.status = NOTHING
        self.DEATHTIME = 36
        self.RESPAWNTIME = 3
        self.DYINGTIME = 3
        self.deathTimer = self.DEATHTIME
        self.respawnTimer = self.RESPAWNTIME
        self.direction = DOWN
        self.action = 0

    def respawn(self, point):
        self.x = point[0]
        self.y = point[1]
        self.status = NOTHING
        if self.respawns > 0:
            self.respawns -= 1
            return True
        else:
            self.dead = True
            return False

    def moveOnPath(self, wallmaze, currentDirection, intentDirection):
        cellWalls = wallmaze.getCellWalls(self.centerx, self.centery)
        # Move in the decided direction
        moveDirection, moveDistance = self.followPath(wallmaze, self.speed, currentDirection, intentDirection, WALLSDICT)
        self.move(moveDirection, moveDistance)
        # Change direction if we're up against a wall
        if moveDirection == intentDirection and cellWalls.getWall(GODIRECTIONS[intentDirection], WALLSDICT) and moveDistance == 0:
            self.aiMoveDecided = False
            self.dirChangeTime = time.time()
        # Consider a new direction each time we enter a cell
        if not cellWalls.collidepoint(self.centerx, self.centery):
            self.aiMoveDecided = False

    def startStatus(self, status):
        self.status = status
        centerX, centerY = self.center
        self.width = PLAYERSIZES[status]
        self.height = PLAYERSIZES[status]
        self.center = (centerX, centerY)
        self.step = STARTCYCLE

    def advanceSteps(self):
        # Now handle all the action cycles
        if self.status == ARRIVING:
            if self.step < PLAYERCYCLES[ARRIVING]:
                self.step += 1
            else:
                self.status = INVULNERABLE
                self.step = STARTCYCLE
                self.direction = DOWN
                self.respawnStartTime = time.time()
        elif self.status == INVULNERABLE:
            if time.time() - self.respawnStartTime > INVULNERABLETIME:
                self.status = STANDING
                self.direction = DOWN
                self.action = ACTIONS[self.direction]
            else:
                self.action = ACTIONS[INVULNERABLE]
                if self.step < PLAYERCYCLES[INVULNERABLE]:
                    self.step += 1
                else:
                    self.step = RESTARTCYCLE
        elif self.status == CASTING:
            if self.step < PLAYERCYCLES[CASTING]:
                self.step += 1
            else:
                self.status = STANDING
                self.step = STARTCYCLE
        elif self.status == DYING:
            if self.step < PLAYERCYCLES[DYING]:
                self.step += 1

    def fireMissile(self):
        return super(Player, self).fireMissile(MISSILESIZE, MISSILESIZE)

    def stepMissile(self, wallmaze, monsterobjs):
        # Move the missile if it has been fired
        if self.missile.fired:
            missileDirection = self.missile.direction
            if self.missile.status == DYING:
                if self.missile.step < MISSILECYCLES[DYING]:
                    self.missile.step += 1
                else:
                    self.missile.step = 0
                    self.stopMissile()
            elif self.missile.status == DEPARTING:
                if self.missile.step < MISSILECYCLES[DEPARTING]:
                    self.missile.step += 1
                else:
                    self.missile.step = 0
                    self.stopMissile()
            elif not wallmaze.contains(self.missile) or not wallmaze.getPathIsOpen(missileDirection, self.missile, WALLSDICT):
                self.missile.status = DEPARTING
                self.missile.step = STARTCYCLE
            else:
                self.moveMissile(missileDirection, MISSILESPEED)
                if self.missile.step < MISSILECYCLES[MOVING]:
                    self.missile.step += 1
                else:
                    self.missile.step = RESTARTCYCLE


class Monster(gameobjects.Shooter):
    def __init__(self, name, dimensions):
        super(Monster, self).__init__(name, dimensions)
        self.step = 0
        self.aiMoveDecided = False
        self.walls = 0
        self.aiMoveDirection = gameobjects.RIGHT
        self.status = ARRIVING
        self.shotdelaytop = 25
        self.shotDelay = self.shotdelaytop
        self.missileFireTime = time.time()
        self.dirChangeTime = time.time()
        self.missile.status = NOTHING
        self.spawnDelay = 0
        self.action = 2
        self.direction = DOWN

    def findPaths(self, wallmaze):
        cellWalls = wallmaze.getCellWalls(self.centerx, self.centery)
        possiblePaths = [UP, LEFT, DOWN, RIGHT]
#        print cellWalls.wallsdict
        if cellWalls.getWall(NORTH, WALLSDICT):
            possiblePaths.remove(UP)
        if cellWalls.getWall(WEST, WALLSDICT):
            possiblePaths.remove(LEFT)
        if cellWalls.getWall(SOUTH, WALLSDICT):
            possiblePaths.remove(DOWN)
        if cellWalls.getWall(EAST, WALLSDICT):
            possiblePaths.remove(RIGHT)
        return possiblePaths

    def aiChooseDirection(self, wallmaze):
        # Decide which direction to go
        cellWalls = wallmaze.getCellWalls(self.centerx, self.centery)
        monsterDirection = self.direction
        if not self.aiMoveDecided:
            chooseDirection = self.findPaths(wallmaze)
            if chooseDirection.count(OPPOSITE[monsterDirection]):
                if time.time() - self.dirChangeTime < DIRCHANGEFREQ:
                    chooseDirection.remove(OPPOSITE[monsterDirection])
                else:
                    self.dirChangeTime = time.time()
            if len(chooseDirection) > 0:
                intentDirection = random.choice(chooseDirection)
            else:
                intentDirection = DOWN
            self.aiMoveDirection = intentDirection
            self.aiMoveDecided = True
        else:
            intentDirection = self.aiMoveDirection
        return intentDirection

    def moveOnPath(self, wallmaze, currentDirection, intentDirection, ):
        cellWalls = wallmaze.getCellWalls(self.centerx, self.centery)
        # Move in the decided direction
        moveDirection, moveDistance = self.followPath(wallmaze, self.speed, currentDirection, intentDirection, WALLSDICT)
        self.move(moveDirection, moveDistance)
        # Change direction if we're up against a wall
        if moveDirection == intentDirection and cellWalls.getWall(GODIRECTIONS[intentDirection], WALLSDICT) and moveDistance == 0:
            self.aiMoveDecided = False
            self.dirChangeTime = time.time()
        # Consider a new direction each time we enter a cell
        if not cellWalls.collidepoint(self.centerx, self.centery):
            self.aiMoveDecided = False
        self.status = MOVING
        self.direction = moveDirection
        self.action = ACTIONS[self.direction]

    def aiDecideFire(self, wallmaze, targets):
        # Return True if monster is in missile range of a player
        if time.time() - self.missileFireTime > FIREFREQUENCY:
            for target in targets:
                if self.targetInRange(wallmaze, target):
                    return True
        return False

    def fireMissile(self):
        super(Monster, self).fireMissile(MISSILESIZE, MISSILESIZE)
        self.missile.centerx = self.centerx
        self.missile.centery = self.centery
        self.missile.name = 'dark-missile'
        self.missile.status = MOVING
        self.missile.direction = self.direction
        self.missileFireTime = time.time()
        self.missile.step = STARTCYCLE-1

    def startStatus(self, status):
        self.status = status
        centerX, centerY = self.center
        self.width = MONSTERSIZES[status]
        self.height = MONSTERSIZES[status]
        self.center = (centerX, centerY)
        self.step = STARTCYCLE

    def advanceSteps(self):
        if self.status == ARRIVING:
            if self.step < MONSTERCYCLES[ARRIVING]:
                self.step += 1
            else:
                self.startStatus(MOVING)
        elif self.status == MOVING:
            if self.step < MONSTERCYCLES[MOVING]:
                self.step += 1
            else:
                self.step = RESTARTCYCLE
        elif self.status == DYING:
            if self.missile.fired:
                self.missile.status = DEPARTING
            if self.step < MONSTERCYCLES[DYING]:
                self.step += 1
            else:
#                self.status = DEAD
                self.startStatus(ORBITING)
                self.orbStartTime = time.time()
        elif self.status == ORBITING:
            if self.step < MONSTERCYCLES[ORBITING]:
                self.step += 1
            # If its spirit is not collected in time, it's resorbed by the
            # Labyrinth
            elif time.time() - self.orbStartTime > ORBTIME:
                self.status = RESPAWNING
                self.step = STARTCYCLE
            else:
                self.step = RESTARTCYCLE
        elif self.status == DEPARTING:
            if self.step < MONSTERCYCLES[DEPARTING]:
                self.step += 1
            else:
                self.status = DEAD

    def stepMissile(self, wallmaze, playerobjs):
        # Move missile if fired
        if self.missile.fired:
            missileDirection = self.missile.direction
            # If missile is dying...
            if self.missile.status in (DEPARTING, DYING):
                if self.missile.step < MISSILECYCLES[DEPARTING]:
                    self.missile.step += 1
                else:
                    self.missile.step = RESTARTCYCLE
                    self.stopMissile()
            else:
                self.moveMissile(missileDirection, MISSILESPEED)
                if self.missile.step < MISSILECYCLES[MOVING]:
                    self.missile.step += 1
                else:
                    self.missile.step = RESTARTCYCLE
            # Otherwise, handle any missile collisions
            missileDirection = self.missile.direction
            if ((not wallmaze.contains(self.missile) or
                    not wallmaze.getPathIsOpen(missileDirection, self.missile, WALLSDICT))
                    and self.missile.status not in (DEPARTING, DYING)):
                # Make missile go away if it hit a wall
                self.missile.status = DEPARTING
                self.missile.step = STARTCYCLE

    def centerSelf(self):
        self.center = self.topleft


class Ghast(Monster):
    def __init__(self, rect):
        self.type = GHAST
        self.points = 100
        self.speed = SPEED1
        super(Ghast, self).__init__(self.type, rect)


# Ghoul is being deprecated and replaced by Wight
class Ghoul(Monster):
    def __init__(self, rect):
        self.type = GHOUL
        self.points = 200
        self.speed = SPEED2
        super(Ghoul, self).__init__(self.type, rect)


class Wight(Monster):
    def __init__(self, rect):
        self.type = WIGHT
        self.points = 200
        self.speed = SPEED2
        super(Wight, self).__init__(self.type, rect)


class Ghost(Monster):
    def __init__(self, rect):
        self.type = GHOST
        self.INVISDELAY = 92
        self.VISDELAY = 92
        self.invisibleTime = self.INVISDELAY
        self.visibleTime = self.VISDELAY
        self.invisibleMode = False
        self.points = 400
        self.speed = SPEED3
        super(Ghost, self).__init__(self.type, rect)


class Shade(Monster):
    def __init__(self, rect):
        self.type = SHADE
        self.points = 500
        self.speed = SPEED3
        super(Shade, self).__init__(self.type, rect)


class LabyrinthLevel(gameobjects.WallMaze):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, wallmap):
        # Figure out the tile size
        cols = len(wallmap[0])
        rows = len(wallmap)
        width = tileSize * cols
        height = tileSize * rows
        # Center the playing field horizonally and slightly lower vertically        
        x = (displayWidth - width) / 2
        y = ((displayHeight - height) / 2) + (displayHeight / 19)
        # Set the persistent sizes
        self.TILESIZE = tileSize
        self.SPRITESIZE = spriteSize
        # Set some player spawning positions
        self.STARTPOSITIONS = ((5, 1),
                               (1, 4),
                               (5, 4),
                               (9, 4))
        # Initialize the parent object
        super(LabyrinthLevel, self).__init__((x, y, width, height), wallmap)
        self.WALLSDICT = WALLSDICT

    def getPlayerSpawnPoint(self):
        x, y = random.choice(self.STARTPOSITIONS)
        (left, top) = self.getLeftTopOfCell(x, y)
#                        player.x += labyrinthObjects.SPRITEOFFSET
#                        player.y += labyrinthObjects.SPRITEOFFSET
        centerX = left + (self.CELLWIDTH / 2)
        centerY = top + (self.CELLHEIGHT / 2)
        return (centerX, centerY)

    def getMonsterSpawnPoint(self):
#        x, y = random.choice(self.MOBPOSITIONS)
        y = random.randint(0, len(self.wallmap)-1)
        x = random.randint(0, len(self.wallmap[0])-1)
        (left, top) = self.getLeftTopOfCell(x, y)
        centerX = left + (self.CELLWIDTH / 2)
        centerY = top + (self.CELLHEIGHT / 2)
        return (centerX, centerY)


class Maze1(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight):
        # Map the walls
        wallmap = [[ NNWW,  NNS,  NNN,  NNE,  NNW,  NNN, NNE,  NNW,  NNN,  NNS,  NNEE ],
                   [ WWE,   NW,   SE,   WW,   EE,   WE,  WW,   EE,   SW,   NE,   EWE ],
                   [ WSW,   EE,   NW,   EE,   SW,   OO,  SE,   WW,   NE,   WW,   ESE ],
                   [ WNW,   SS,   EE,   SW,   NN,   SS,  NN,   SE,   WW,   SS,   ENE ],
                   [ WWW,   NS,   OO,   NE,   WW,   NS,  EE,   NW,   OO,   NS,   EEE ],
                   [ WWE,   NW,   EE,   WW,   SS,   NN,  SS,   EE,   WW,   NE,   EWE ],
                   [ SSWW,  SSE,  SSW,  SSS,  SNS,  SSS, SNS,  SSS,  SSE,  SSW,  SSEE ]]
        # Initialize the parent object
        super(Maze1, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap)


def getMonster(type, x, y):
    if type == GHAST:
        monster = Ghast((x, y, MONSTERSIZE, MONSTERSIZE))
    elif type == WIGHT:
        monster = Wight((x, y, MONSTERSIZE, MONSTERSIZE))
    elif type == SHADE:
        monster = Shade((x, y, MONSTERSIZE, MONSTERSIZE))
    return monster
