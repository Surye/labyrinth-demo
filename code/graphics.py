# Copyright 2015, 2016 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame
import time
import copy
import random

random.seed()

AQUA     = (  0, 255, 255)
BLACK    = (  0,   0,   0)
BLUE     = (  0,   0, 255)
FUCHSIA  = (255,   0, 255)
GRAY     = (128, 128, 128)
GREEN    = (  0, 255,   0)
LIME     = (  0, 255,   0)
MAROON   = (128,   0,   0)
NAVYBLUE = (  0,   0, 128)
OLIVE    = (128, 128,   0)
PURPLE   = (128,   0, 128)
RED      = (255,   0,   0)
SILVER   = (192, 192, 192)
TEAL     = (  0, 128, 128)
WHITE    = (255, 255, 255)
YELLOW   = (255, 255,   0)

FULLSCREENPARAM = '--fullscreen'
WINDOWEDPARAM = '--windowed'

FULLSCREEN = pygame.FULLSCREEN
WINDOWED = 0

DEFAULT = 'default'
FONTNAME = pygame.font.get_default_font()
SMALLFONTSIZE = 14
NORMALFONTSIZE = 18
NORMALGAP = 4
LARGEFONTSIZE = 48
OVERFONTSIZE = 72
FONTSIZE = NORMALFONTSIZE
NORMALFONTCOLOR = WHITE
DARKFONTCOLOR = SILVER

# Experimenting with sprite flickering, to reduce update load
MAXSPRITES = 2
ODDS = 1
EVENS = 0

class Graphics():
    def __init__(self, displaywidth, displayheight, displaycaption, fullscreen=False, bgcolor=BLACK):
        self.WINDOWWIDTH = displaywidth
        self.WINDOWHEIGHT = displayheight
        self.HALFWIDTH = displaywidth/2
        self.HALFHEIGHT = displayheight/2
#        try:
#            iconImage = pygame.image.load(displayicon)
#            pygame.display.set_icon(iconImage)
#        except:
#            print "Display icon filname not found"
        pygame.display.set_caption(displaycaption)
        if fullscreen:
            flags=FULLSCREEN
        else:
            flags=0
        self.DISPLAYSURF = pygame.display.set_mode((self.WINDOWWIDTH, self.WINDOWHEIGHT), flags)
        self.BGCOLOR = bgcolor
        self.FONTSIZE = FONTSIZE
        self.FONTNAME = FONTNAME
        self.BACKGROUNDSURF = self.DISPLAYSURF.copy()
        self.TILESETS = {}
        self.SPRITEIMAGES = {}
        self.dirtyRects = []
        self.oldRects = []
        self.blank()
        self.evensOrOdds = EVENS

    def toggleFullscreen(self):
        pygame.display.toggle_fullscreen()

    def addSprite(self, name, filename, clipSize, totalSteps):
#        clipSize = 18
        try:
            spriteImage = pygame.image.load(filename).convert_alpha()
#            clipSize = spriteImage.get_height() / directions
            newSprite = {'image': spriteImage,
                         'clipwidth': clipSize,
                         'clipheight': clipSize,
                         'clipsteps': totalSteps}
        except:
            font = pygame.font.SysFont('Calibri', 12, True, False)
            image = font.render('file missing', True, WHITE)
            newSprite = {'image': image,
                         'clipwidth': image.get_width(),
                         'clipheight': image.get_height(),
                         'clipsteps': 1}
            print(filename)
        imgWidth = newSprite['image'].get_width()
        if imgWidth < clipSize:
            numFrames = 1
        else:
            numFrames = imgWidth / clipSize
#        print name, imgWidth, clipSize, numFrames
        stepsPerFrame = totalSteps / numFrames
        newSprite['clipsteps'] = stepsPerFrame
        self.SPRITEIMAGES[name] = newSprite

    def addTileSet(self, filename, name, tileWidth, tileHeight):
        newTileSet = {'image': pygame.image.load(filename),
                     'clipwidth': tileWidth,
                     'clipheight': tileHeight}
        self.TILESETS[name] = newTileSet
        self.TILESET = pygame.image.load(filename)
        self.TILEWIDTH = tileWidth
        self.TILEHEIGHT = tileHeight

    def tileMap(self, normalTiles, mapRect, tileMap):
        tileWidth = mapRect.width / len(tileMap[0])
        tileHeight = mapRect.height / len(tileMap)
        if normalTiles in self.TILESETS:
            imagesDict = self.TILESETS[normalTiles]
        else:
            font = pygame.font.SysFont('Calibri', 12, True, False)
            image = font.render('name missing', True, WHITE)
            print("name missing: " + normalTiles)
            imagesDict = {'image': image,
                          'clipwidth': image.get_width(),
                          'clipheight': image.get_height()}
        tilesImage = imagesDict['image'].copy()
        clipWidth = imagesDict['clipwidth']
        clipHeight = imagesDict['clipheight']
        tileColumns = tilesImage.get_width()/clipWidth
        tileRows = tilesImage.get_height()/clipHeight
        for cy in range(len(tileMap)):
            for cx in range(len(tileMap[cy])):
                tileNum = tileMap[cy][cx]
                cellRect = pygame.Rect(mapRect.x + (tileWidth * cx), mapRect.y + (tileHeight * cy), tileWidth, tileHeight)
                tx = tileNum
                ty = 0
                while tx > tileColumns-1:
                    tx -= tileColumns
                    ty += 1
                if (tx*clipWidth)+clipWidth-1 < tilesImage.get_width() and (ty*clipHeight)+clipHeight-1 < tilesImage.get_height():
                    wallImage = tilesImage.subsurface(tx*clipWidth, ty*clipHeight, clipWidth, clipHeight)
                    self.DISPLAYSURF.blit(wallImage, cellRect)
                else:
                    wallImage = tilesImage.subsurface(0, 0, clipWidth, clipHeight)
                    self.DISPLAYSURF.blit(wallImage, cellRect)
                    pygame.draw.rect(self.DISPLAYSURF, WHITE, mapRect, 1)
                    pygame.draw.rect(self.DISPLAYSURF, WHITE, cellRect, 1)
        self.addDirtyRect(mapRect)
        self.BACKGROUNDSURF = self.DISPLAYSURF.copy()

    def tileMapRandom(self, tileSetName, mapRect):
        imagesDict = self.TILESETS[tileSetName]
        tileWidth = imagesDict['clipwidth']
        tileHeight = imagesDict['clipheight']
        tileMap = []
        for ty in range(0, mapRect.height/tileHeight):
            newRow = []
            for tx in range(0, mapRect.width/tileWidth):
                newRow.append(random.randint(0, 24))
            tileMap.append(newRow)
        self.tileMap(tileSetName, mapRect, tileMap)
        
    def getSpriteClip(self, spritename, sequence, frame):
        if frame > 0:
            frame -= 1
#            spritename = spriteObj.name + '-' + spriteObj.status
        if spritename in self.SPRITEIMAGES:
            imagesDict = self.SPRITEIMAGES[spritename]
        else:
            font = pygame.font.SysFont('Calibri', 12, True, False)
            image = font.render('name missing', True, WHITE)
            print("name missing: " + spritename)
            imagesDict = {'image': image,
                          'clipwidth': image.get_width(),
                          'clipheight': image.get_height(),
                          'clipsteps': 1}
        spriteImage = imagesDict['image'].copy()
        clipWidth = imagesDict['clipwidth']
        clipHeight = imagesDict['clipheight']
        clipSteps = imagesDict['clipsteps']
        stepsPerFrame = clipSteps
        # If there is no row of animations, just use the first row
        if (sequence * clipHeight)+clipHeight > spriteImage.get_height():
            sequence = 0
        if stepsPerFrame > 0:
            frame = frame / stepsPerFrame
        else:
            frame = 0
        # If the animation doesn't have enough frames, just display the first frame
        if (frame * clipWidth)+clipWidth > spriteImage.get_width():
            frame = (spriteImage.get_width() / clipWidth) - 1
        # If there is no approprate are in the image to clip, just draw a white rectangle
        if (frame * clipWidth)+clipWidth > spriteImage.get_width() or (sequence * clipHeight)+clipHeight > spriteImage.get_height():
            clipImage = pygame.Surface((clipwidth, clipheight))
            pygame.draw.rect(clipImage, WHITE, (0, 0, clipwidth, clipheight), 1)
        else:
            clipImage = spriteImage.subsurface(((frame * clipWidth), (sequence * clipHeight), clipWidth, clipHeight))
        return clipImage

    def drawAllSprites(self, spriteDicts):
        # drawAllSprites() is designed to draw and erase ALL of the 
        # sprites frame-by-frame quickly and efficiently. It keeps track 
        # of previously- drawn rects in order to erase them, because the 
        # list only has the new positions of each sprite. It then tries 
        # to unionize any rects that are touching, including the old 
        # rects that have been erased and the new rects that have been 
        # drawn; this reduces the number of screen updates significantly 
        # per-frame if the sprites move incrementally.
        
        # I'm also trying to implement a "flicker" effect in order to 
        # cut the number of updates in half by drawing some sprites in 
        # one frame and the rest next frame.
        
        # Sort the new sprites and get a simple spritedict
        reOrderedDicts = sorted(spriteDicts, key=lambda k: k['y'])
        drawTheseSpriteDicts = []
        for spriteDict in reOrderedDicts:
            spritename = spriteDict['name']
            animationSeq = spriteDict['direction']
            animationFrame = spriteDict['step']
            clipImage = self.getSpriteClip(spritename, animationSeq, animationFrame)
            clipWidth = clipImage.get_width()
            clipHeight = clipImage.get_height()
            spriteRect = pygame.Rect((spriteDict['x'], spriteDict['y'], clipWidth, clipHeight))
            simpleSpriteDict = {'image': clipImage, 'rect': spriteRect}
            drawTheseSpriteDicts.append(simpleSpriteDict)
        # Erase all the old sprites
        dirtyRects = []
        for oldRect in self.oldRects:
            if self.BACKGROUNDSURF.get_rect().contains(oldRect):
                cleanImage = self.BACKGROUNDSURF.subsurface(oldRect)
                self.DISPLAYSURF.blit(cleanImage, oldRect)
            dirtyRects.append(oldRect)
        # Draw all the new sprites
        self.oldRects = []
        for spriteDict in drawTheseSpriteDicts:
            self.DISPLAYSURF.blit(spriteDict['image'], spriteDict['rect'])
            # If self.FOREGROUNDSURF.get_rect().contains(spriteDict['rect'])
                # Draw an image from self.FOREGROUNDSURF
            dirtyRects.append(spriteDict['rect'])
            self.oldRects.append(spriteDict['rect'])
        # Figure out what to add to dirtyrects for update
        for dirtyRect in dirtyRects:
            self.addDirtyRect(dirtyRect)
#        for i in range(1, len(dirtyRects)):
#            print('i = ' + str(i) + ' / ' + str(len(dirtyRects)))
#            for j in range(i):
#                print('j = ' + str(j))
#                if dirtyRects[i].colliderect(dirtyRects[j]):
#                    self.addDirtyRect(dirtyRects[i].union(dirtyRects[j]))
#                elif i + 1 == len(dirtyRects):
#                    self.addDirtyRect(dirtyRects[j])

    def setFont(self, fontname, fontsize):
        self.FONTSIZE = fontsize
        self.FONTNAME = fontname

    def getTextSurf(self, text, textcolor, x, y, fontsize, fontname):
        if fontname == DEFAULT:
            font = pygame.font.SysFont(FONTNAME, fontsize, True, False)
        else:
            font = pygame.font.Font(fontname, fontsize)
        textSurf = font.render(text, True, textcolor)
        textRect = textSurf.get_rect()
        return textSurf, textRect

    def showText(self, text, x, y, fontcolor=NORMALFONTCOLOR, fontsize=FONTSIZE, fontname=FONTNAME):
        textSurf, textRect = self.getTextSurf(text, fontcolor, x, y, fontsize, fontname)
        textRect.topleft = (x, y)
        self.DISPLAYSURF.blit(textSurf, textRect)
        self.addDirtyRect(textRect)

    def showTextCentered(self, text, centerx, centery, fontcolor=NORMALFONTCOLOR, fontsize=FONTSIZE, fontname=FONTNAME):
        textSurf, textRect = self.getTextSurf(text, fontcolor, centerx, centery, fontsize, fontname)
        textRect.center = (centerx, centery)
        self.DISPLAYSURF.blit(textSurf, textRect)
        self.addDirtyRect(textRect)

    def showTextBox(self, text, textcolor, boxcolor, x, y, width, height, fontsize=FONTSIZE, fontname=FONTNAME):
        textSurf, textRect = self.getTextSurf(text, textcolor, x, y, fontsize, fontname)
        boxRect = pygame.Rect((x, y, width, height))
        textRect.topleft = (x, y)
        boxRect.topleft = (x, y)
        pygame.draw.rect(self.DISPLAYSURF, boxcolor, boxRect, 0)
        self.DISPLAYSURF.blit(textSurf, textRect)
        self.addDirtyRect(boxRect)

    def showTextBoxCentered(self, text, textcolor, boxcolor, centerx, centery, width, height, fontsize=FONTSIZE, fontname=FONTNAME):
        textSurf, textRect = self.getTextSurf(text, textcolor, centerx, centery, fontsize, fontname)
        boxRect = pygame.Rect((centerx, centery, width, height))
        textRect.center = (centerx, centery)
        boxRect.center = (centerx, centery)
        pygame.draw.rect(self.DISPLAYSURF, boxcolor, boxRect, 0)
        self.DISPLAYSURF.blit(textSurf, textRect)
        self.addDirtyRect(boxRect)

    def showList(self, list, textcolor, x, y, fontsize=FONTSIZE, fontname=FONTNAME):
        listRect = pygame.Rect((x, y, 1, 1))
        for index, item in enumerate(list):
            textSurf, textRect = self.getTextSurf(item, textcolor, x, y, fontsize, fontname)
            textRect.topleft = (x, y+((fontsize+NORMALGAP)*index))
            self.DISPLAYSURF.blit(textSurf, textRect)
            listRect = listRect.union(textRect)
        # Add the whole list as one dirty-rect
        self.addDirtyRect(listRect)

    def showListCentered(self, list, x, y, fontcolor=NORMALFONTCOLOR, fontsize=FONTSIZE, fontname=FONTNAME):
        listRect = pygame.Rect((x, y, 1, 1))
        for index, item in enumerate(list):
            textSurf, textRect = self.getTextSurf(item, fontcolor, x, y, fontsize, fontname)
            textRect.center = (x, y+((fontsize+NORMALGAP)*index))
            if self.DISPLAYSURF.get_rect().colliderect(textRect):
                self.DISPLAYSURF.blit(textSurf, textRect)
                listRect = listRect.union(textRect)
        # Add the whole list as one dirty-rect
        self.addDirtyRect(listRect)

    def showMenuList(self, list, textcolor, boxcolor, x, y, width, height, cursor):
        newlist = []
        for item in list:
            index = list.index(item)
            if index == cursor:
                newlist.append("-> " + item + " <-")
            else:
                newlist.append(item)
        boxRect = pygame.Rect((x, y, width, height))
        boxRect.centerx = x
        boxRect.top = y-FONTSIZE
        pygame.draw.rect(self.DISPLAYSURF, boxcolor, boxRect, 0)
        self.showListCentered(tuple(newlist), x, y, textcolor)
        self.addDirtyRect(boxRect)

    def showImage(self, filename, x, y):
        imageSurf = pygame.image.load(filename)
        imageRect = pygame.Rect(imageSurf.get_rect())
        imageRect.topleft = (x, y)
        self.DISPLAYSURF.blit(imageSurf, imageRect)
        self.dirtyRects.append(imageRect)

    def showImageCentered(self, filename, x, y):
        imageSurf = pygame.image.load(filename)
        imageRect = pygame.Rect(imageSurf.get_rect())
        imageRect.center = (x, y)
        self.DISPLAYSURF.blit(imageSurf, imageRect)
        self.dirtyRects.append(imageRect)

    def addDirtyRect(self, rect):
        newDirtyRect = rect
        # Iterate in reverse since we'll be removing items
        for i in range(len(self.dirtyRects)-1, -1, -1):
            if newDirtyRect.colliderect(self.dirtyRects[i]):
                newDirtyRect = newDirtyRect.union(self.dirtyRects.pop(i))
        self.dirtyRects.append(newDirtyRect)

    def update(self):
        print("Dirtyrects: " + str(len(self.dirtyRects)))
        print(self.dirtyRects)
        pygame.display.update(self.dirtyRects)
        self.dirtyRects = []

    def blank(self):
        self.DISPLAYSURF.fill(self.BGCOLOR)
        self.textRects = {}
        self.addDirtyRect(self.DISPLAYSURF.get_rect())

    def wallMaze(self, level, wallmaze):
        wallmap = wallmaze.wallmap
        wallcolor = GRAY
        pygame.draw.rect(self.DISPLAYSURF, wallcolor, (0, 0, self.WINDOWWIDTH, self.WINDOWHEIGHT), 1)
        for cy in range(len(wallmap)):
            for cx in range(len(wallmap[cy])):
                cellrect = wallmaze.getCellRect(cx, cy)
                if wallmap[cy][cx] & WEST:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.left, cellrect.top, 1, cellrect.height), 1)
                if wallmap[cy][cx] & NORTH:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.left, cellrect.top, cellrect.width, 1), 1)
                if wallmap[cy][cx] & SOUTH:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.left, cellrect.bottom-1, cellrect.width, 1), 1)
                if wallmap[cy][cx] & EAST:
                    pygame.draw.rect(self.DISPLAYSURF, wallcolor, (cellrect.right-1, cellrect.top, 1, cellrect.height), 1)
