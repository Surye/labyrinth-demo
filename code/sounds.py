# Copyright 2015 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, time, random

NOSOUNDPARAM = '--nosound'
NOMUSICPARAM = '--nomusic'

# Pre-initialize sound mixer
pygame.mixer.pre_init(44100, -16, 2, 2048)

class Sounds():
    def __init__(self):
        self.SOUNDEFFECTS = {}
        self.SOUNDLISTS = {}
        self.PLAYLIST = []
        self.nosound = False
        self.nomusic = False

    def addSoundEffect(self, name, filename):
        self.SOUNDEFFECTS[name] = pygame.mixer.Sound(filename)

    def playSoundEffect(self, name):
        if not self.nosound:
            self.SOUNDEFFECTS[name].play()

    def addSoundList(self, name, prefix, number, suffix):
        newSoundList = []
        for i in range(0, number):
            newSoundList.append(pygame.mixer.Sound(prefix + str(i) + suffix))
        self.SOUNDLISTS[name] = newSoundList

    def playRandomSound(self, name):
        if not self.nosound:
            n = random.randint(0, len(self.SOUNDLISTS[name])-1)
            soundList = self.SOUNDLISTS[name]
            soundList[n].play()

    def addPlaylist(self, prefix, number, suffix):
        self.PLAYLIST = []
        for i in range(0, number):
            self.PLAYLIST.append(prefix + str(i) + suffix)
        self.playlistPos = 1

    def startPlaylist(self):
        if not pygame.mixer.music.get_busy():
            pygame.mixer.music.load(self.PLAYLIST[self.playlistPos-1])
            if not self.nosound and not self.nomusic:
                pygame.mixer.music.play(-1)
        else:
            pygame.mixer.music.unpause()

    def advancePlaylist(self):
        self.playlistPos += 1
        self.playlistPos = self.playlistPos % len(self.PLAYLIST)

    def isPlaylistStarted(self):
        if pygame.mixer.music.get_busy():
            return True
        else:
            return False

    def stopPlaylist(self):
        pygame.mixer.music.stop()
