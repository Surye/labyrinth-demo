# Copyright 2015, 2016 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, time, pygame

PLAYER1 = 0
PLAYER2 = 1
PLAYER3 = 2
PLAYER4 = 3

BUTTON1 = 0
BUTTON2 = 1
BUTTON12 = 11
BUTTON13 = 12
BUTTON14 = 13
BUTTON15 = 14
LEFTRIGHT = 0
UPDOWN = 1

LEFT = 'left'
UP = 'up'
DOWN = 'down'
RIGHT = 'right'

REPEATFREQ = 0.25

ORTHOGONAL = True

# Default controlkeys
ESCAPEKEY = -1
NOKEY = 0
HELPKEY = 1
OPTIONKEY = 2
SELECTKEY = 3
STARTKEY = 4
RESETKEY = 5

pygame.joystick.init()
# Initialize joysticks
JOYSTICK_COUNT = pygame.joystick.get_count()
if JOYSTICK_COUNT == 0:
    # No joysticks!
    print("No joysticks connected.")
else:
    # Initialize each joystick
    JOYSTICKS = [pygame.joystick.Joystick(x) for x in range(JOYSTICK_COUNT)]
    for i in range(JOYSTICK_COUNT):
        JOYSTICKS[i].init()

class KeyMap():
    def __init__(self):
        # Player 1
        self.K_a = False
        self.K_s = False
        self.K_w = False
        self.K_e = False
        self.K_d = False
        self.K_f = False
        self.K_LCTRL = False
        self.K_LSHIFT = False
        # Player 2
        self.K_i = False
        self.K_j = False
        self.K_k = False
        self.K_l = False
        self.K_SPACE = False
        self.K_RETURN = False
        # Player 3
        self.K_LEFT = False
        self.K_UP = False
        self.K_DOWN = False
        self.K_RIGHT = False
        self.K_RCTRL = False
        self.K_RSHIFT = False
        # Player 4
        # Console Keys
        self.K_ESCAPE = False
        

class JoyMap():
    def __init__(self):
        # Initialize control values
        self.ABS_X = [0, 0, 0, 0]
        self.ABS_Y = [0, 0, 0, 0]
        self.ABS_HAT0X = [0, 0, 0, 0]
        self.ABS_HAT0Y = [0, 0, 0, 0]
        self.BTN_A = [False, False, False, False]
        self.BTN_B = [False, False, False, False]
        # Alternate d-pad inputs for when HAT0 is reported as buttons
        # In order: left, right, up, down
        self.BTN_TRIGGER_HAPPY1 = [False, False, False, False]
        self.BTN_TRIGGER_HAPPY2 = [False, False, False, False]
        self.BTN_TRIGGER_HAPPY3 = [False, False, False, False]
        self.BTN_TRIGGER_HAPPY4 = [False, False, False, False]


# A Controller() is a virtual arcade console, one per player. It keeps
# track of the position of various buttons, 4-way sticks, throttles,
# axes, etc. It can also keep track of a repeat frequency so holding
# e.g. DOWN on the stick will scroll through a menu at a reasonable
# speed, if called for.

class Controller():
    def __init__(self):
        self.buttons = [False, False]
        self.button_timer = [time.time(), time.time()]
        self.button_repeat = [True, True]
        self.stick = [0, 0]
        self.stick_timer = [time.time(), time.time()]
        self.stick_repeat = [True, True]
        self.freq_reset = time.time()
        self.stickUp = False
        self.stickDown = False
        self.stickLeft = False
        self.stickRight = False
        self.stickRepeat = True


# ControlMap() aggregates events from various sources (keyboard, mouse,
# joystick) and compiles it all into a single control set per player.
# Think of this as a virtual controller akin to an arcade machine. Each
# individual input is kept track of so several different
# keys/buttons/etc. can determine the state of e.g. the virtual 4-way
# stick and/or a set of 6 buttons without confusion.

class ControlMap():
    def __init__(self):
        self.keymap = KeyMap()
        self.joymap = JoyMap()
        self.controllers = [Controller(), Controller()]
        pygame.mouse.set_visible(False)
        self.PLAYER1 = 0
        self.PLAYER2 = 1
        self.PLAYER3 = 2
        self.PLAYER4 = 3
        self.BUTTON1 = 0
        self.BUTTON2 = 1

    def process_events(self):

        # if one of the function keys is pressed, just return the value
        functionkeys = { pygame.K_ESCAPE : -1,
                         pygame.K_F1 : 1,
                         pygame.K_F2 : 2,
                         pygame.K_F3 : 3,
                         pygame.K_F4 : 4,
                         pygame.K_F5 : 5,
                         pygame.K_F6 : 6,
                         pygame.K_F7 : 7,
                         pygame.K_F8 : 8,
                         pygame.K_F9 : 9,
                         pygame.K_F10 : 10,
                         pygame.K_F11 : 11,
                         pygame.K_F12 : 12,
        }
        # gamecontrol_keys maps pygame keys to names of ControlMap keys.
        gamecontrol_keys = { pygame.K_LEFT : "K_LEFT",
                             pygame.K_LEFT : "K_LEFT",
                             pygame.K_UP : "K_UP",
                             pygame.K_DOWN : "K_DOWN",
                             pygame.K_RIGHT : "K_RIGHT",
                             pygame.K_RCTRL : "K_RCTRL",
                             pygame.K_RSHIFT :"K_RSHIFT",
                             pygame.K_i : "K_i",
                             pygame.K_j : "K_j",
                             pygame.K_k : "K_k",
                             pygame.K_l : "K_l",
                             pygame.K_SPACE : "K_SPACE",
                             pygame.K_RETURN : "K_RETURN",
                             pygame.K_ESCAPE : "K_ESCAPE",
                             pygame.K_a : "K_a",
                             pygame.K_s : "K_s",
                             pygame.K_w : "K_w",
                             pygame.K_e : "K_e",
                             pygame.K_d : "K_d",
                             pygame.K_f : "K_f",
                             pygame.K_LCTRL : "K_LCTRL",
                             pygame.K_LSHIFT : "K_LSHIFT",
        }
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return -1
            if event.type == pygame.KEYDOWN:
                if event.key in functionkeys:
                    return functionkeys[event.key]
                elif event.key in gamecontrol_keys:
                    setattr(self.keymap, gamecontrol_keys[event.key], True)
            elif event.type == pygame.KEYUP:
                if event.key in gamecontrol_keys:
                    setattr(self.keymap, gamecontrol_keys[event.key], False)
            elif event.type == pygame.JOYBUTTONDOWN or event.type == pygame.JOYBUTTONUP:
                for j in range(len(JOYSTICKS)):
                    joystick = JOYSTICKS[j]
                    if j <= PLAYER4:
                        for b in range(joystick.get_numbuttons()):
                            if b == BUTTON1:
                                self.joymap.BTN_A[j] = joystick.get_button(b)
                            elif b == BUTTON2:
                                self.joymap.BTN_B[j] = joystick.get_button(b)
                            elif b == BUTTON12:
                                self.joymap.BTN_TRIGGER_HAPPY1[j] = joystick.get_button(b)
                            elif b == BUTTON13:
                                self.joymap.BTN_TRIGGER_HAPPY2[j] = joystick.get_button(b)
                            elif b == BUTTON14:
                                self.joymap.BTN_TRIGGER_HAPPY3[j] = joystick.get_button(b)
                            elif b == BUTTON15:
                                self.joymap.BTN_TRIGGER_HAPPY4[j] = joystick.get_button(b)
            elif event.type == pygame.JOYAXISMOTION:
                for j in range(len(JOYSTICKS)):
                    joystick = JOYSTICKS[j]
                    if j <= PLAYER4:
                        if joystick.get_numaxes() > 1:
                            self.joymap.ABS_X[j] = joystick.get_axis(0)
                            self.joymap.ABS_Y[j] = joystick.get_axis(1)
            elif event.type == pygame.JOYHATMOTION:
                for j in range(len(JOYSTICKS)):
                    joystick = JOYSTICKS[j]
                    if j <= PLAYER4:
                        if joystick.get_numhats() > 0:
                            self.joymap.ABS_HAT0X[j] = joystick.get_hat(0)[0]
                            self.joymap.ABS_HAT0Y[j] = joystick.get_hat(0)[1]
        player1 = self.controllers[PLAYER1]
        player2 = self.controllers[PLAYER2]
        # Figure out the position of the joystick
        player1.stick[LEFTRIGHT] = 0
        player1.stick[UPDOWN] = 0
        player2.stick[LEFTRIGHT] = 0
        player2.stick[UPDOWN] = 0
        # For Player 1
        # Move stick
        player1.stickUp = False
        player1.stickDown = False
        player1.stickLeft = False
        player1.stickRight = False
        if self.keymap.K_a or self.keymap.K_LEFT or self.joymap.BTN_TRIGGER_HAPPY1[PLAYER1] or self.joymap.ABS_X[PLAYER1] < -0.5 or self.joymap.ABS_HAT0X[PLAYER1] < 0:
            player1.stick[LEFTRIGHT] -= 1
            player1.stickLeft = True
        if self.keymap.K_d or self.keymap.K_RIGHT or self.joymap.BTN_TRIGGER_HAPPY2[PLAYER1] or self.joymap.ABS_X[PLAYER1] > 0.5 or self.joymap.ABS_HAT0X[PLAYER1] > 0:
            player1.stick[LEFTRIGHT] += 1
            player1.stickRight = True
        if self.keymap.K_w or self.keymap.K_UP or self.joymap.BTN_TRIGGER_HAPPY3[PLAYER1] or self.joymap.ABS_Y[PLAYER1] < -0.5 or self.joymap.ABS_HAT0Y[PLAYER1] > 0:
            player1.stick[UPDOWN] -= 1
            player1.stickUp = True
        if self.keymap.K_s or self.keymap.K_DOWN or self.joymap.BTN_TRIGGER_HAPPY4[PLAYER1] or self.joymap.ABS_Y[PLAYER1] > 0.5 or self.joymap.ABS_HAT0Y[PLAYER1] < 0:
            player1.stick[UPDOWN] += 1
            player1.stickDown = True
        # Repeat frequency for sticks
        if player1.stick[UPDOWN] == 0 and player1.stick[LEFTRIGHT] == 0:
            player1.stickTimer = player1.freq_reset
            player1.stickRepeat = True
        elif time.time() - player1.stickTimer > REPEATFREQ:
            player1.stickTimer = time.time()
            player1.stickRepeat = True
        else:
            player1.stickRepeat = False
        # Press buttons
        if self.keymap.K_LCTRL or self.keymap.K_RCTRL or self.joymap.BTN_A[PLAYER1]:
            player1.buttons[BUTTON1] = True
        else:
            player1.buttons[BUTTON1] = False
        if self.keymap.K_LSHIFT or self.keymap.K_RSHIFT or self.joymap.BTN_B[PLAYER1]:
            player1.buttons[BUTTON2] = True
        else:
            player1.buttons[BUTTON2] = False
        # Repeat frequency for buttons
        for b in range(len(player1.buttons)):
            if not player1.buttons[b]:
                player1.button_timer[b] = player1.freq_reset
                player1.button_repeat[b] = True
            elif time.time() - player1.button_timer[b] > REPEATFREQ:
                player1.button_timer[b] = time.time()
                player1.button_repeat[b] = True
            else:
                player1.button_repeat[b] = False
        # for player 2
        player2.stickUp = False
        player2.stickDown = False
        player2.stickLeft = False
        player2.stickRight = False
        if self.keymap.K_j or self.joymap.BTN_TRIGGER_HAPPY1[PLAYER2] or self.joymap.ABS_X[PLAYER2] < -0.5 or self.joymap.ABS_HAT0X[PLAYER2] < 0:
            player2.stick[LEFTRIGHT] -= 1
            player2.stickLeft = True
        if self.keymap.K_l or self.joymap.BTN_TRIGGER_HAPPY2[PLAYER2] or self.joymap.ABS_X[PLAYER2] > 0.5 or self.joymap.ABS_HAT0X[PLAYER2] > 0:
            player2.stick[LEFTRIGHT] += 1
            player2.stickRight = True
        if self.keymap.K_i or self.joymap.BTN_TRIGGER_HAPPY3[PLAYER2] or self.joymap.ABS_Y[PLAYER2] < -0.5 or self.joymap.ABS_HAT0Y[PLAYER2] > 0:
            player2.stick[UPDOWN] -= 1
            player2.stickUp = True
        if self.keymap.K_k or self.joymap.BTN_TRIGGER_HAPPY4[PLAYER2] or self.joymap.ABS_Y[PLAYER2] > 0.5 or self.joymap.ABS_HAT0Y[PLAYER2] < 0:
            player2.stick[UPDOWN] += 1
            player2.stickDown = True
        # Repeat frequency for sticks
        if player2.stick[UPDOWN] == 0:
            player2.stick_timer[UPDOWN] = player2.freq_reset
            player2.stickRepeat = True
        elif time.time() - player2.stick_timer[UPDOWN] > REPEATFREQ:
            player2.stick_timer[UPDOWN] = time.time()
            player2.stickRepeat = True
        else:
            player2.stickRepeat = False
        # Press buttons
        if self.keymap.K_SPACE or self.joymap.BTN_A[PLAYER2]:
            player2.buttons[BUTTON1] = True
        else:
            player2.buttons[BUTTON1] = False
        if self.keymap.K_RETURN or self.joymap.BTN_B[PLAYER2]:
            player2.buttons[BUTTON2] = True
        else:
            player2.buttons[BUTTON2] = False
        # Repeat frequency for buttons
        for b in range(len(player2.buttons)):
            if not player2.buttons[b]:
                player2.button_timer[b] = player2.freq_reset
                player2.button_repeat[b] = True
            elif time.time() - player2.button_timer[b] > REPEATFREQ:
                player2.button_timer[b] = time.time()
                player2.button_repeat[b] = True
            else:
                player2.button_repeat[b] = False

    def getController(self, player):
        return self.controllers[player]

    def getButton(self, player, button):
        if self.controllers[player].buttons[button]:
            if self.controllers[player].button_repeat[button]:
                return True
        return False

    def getAnyButton(self, player):
        for button in (BUTTON1, BUTTON2):
            if self.controllers[player].buttons[button]:
                if self.controllers[player].button_repeat[button]:
                    return True
        return False

    def getAnyButtonAll(self):
        for player in (PLAYER1, PLAYER2):
            for button in (BUTTON1, BUTTON2):
                if self.controllers[player].buttons[button]:
                    if self.controllers[player].button_repeat[button]:
                        return True
        return False

    def getStick(self, player, direction):
        if self.controllers[player].stickRepeat:
            if direction == UP:
                if self.controllers[player].stick[UPDOWN] < 0:
                    return True
            elif direction == DOWN:
                if self.controllers[player].stick[UPDOWN] > 0:
                    return True
            if direction == LEFT:
                if self.controllers[player].stick[LEFTRIGHT] < 0:
                    return True
            elif direction == RIGHT:
                if self.controllers[player].stick[LEFTRIGHT] > 0:
                    return True
        return False

    def getStickDirection(self, player, orthogonal=False):
        if self.controllers[player].stick[UPDOWN] < 0:
            return UP
        elif self.controllers[player].stick[UPDOWN] > 0:
            return DOWN
        elif self.controllers[player].stick[LEFTRIGHT] < 0:
            return LEFT
        elif self.controllers[player].stick[LEFTRIGHT] > 0:
            return RIGHT

    def update(self):
        consoleKey = self.process_events()
        return self


# FOOTNOTE
# Convergence notes about Human Interface Devices
# Computer programs should respond to all reasonable inputs
# Note that some keyboards won't recognize certain keys being pressed
# simultaneously
# The following list assuming Player 1 only
# 1st Joystick = D-Pad = Arrow Keys = ?Mouse Move?
# 1st Button = A = Spacebar = Mouse Button 1
# 2nd Button = B = Backspace/Ctrl
# 3rd Button = X = X Key/?Shift
# 4th Button = Y = Z Key
# 5th Button = L = ?Left Shift
# 6th Button = R = ?Right Shift
# 7th Button = Select = Tab Key
# 8th Button = Start = Enter Key
# 9th Button = Guide(Mode) = Escape Key/Super Key
