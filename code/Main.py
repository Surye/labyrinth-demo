# Copyright 2015, 2016 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame
import sys
import time
import random
from pygame.locals import *

# Import engine and game modules
import graphics
import sounds
import controls
import gameobjects
import labyrinthGame
import labyrinthObjects

# Constant values
DISPLAYWIDTH = 640
DISPLAYHEIGHT = 480
HALFWIDTH = DISPLAYWIDTH/2
HALFHEIGHT = DISPLAYHEIGHT/2
TOPOFSCREEN = 20
BOTTOMOFSCREEN = 460
LEFTOFSCREEN = 20
RIGHTOFSCREEN = 620
FPS = 30
DEBUG = '--debug'
BGCOLOR = (33, 30, 39)

# Game mode constants
PLEASEWAIT = "Please Wait"
POWEREDBY = "Powered By"
LICENSE = "License"
TITLE = "Title"
ONEPLAYER = "1 Player"
TWOPLAYER = "2 Player"
GAMEMODE = "Game Mode: "
PLAYERSMODE = "Players: "
ARCADEMODE = "Arcade"
VERSUSMODE = "Versus"
QUESTMODE = "Quest"
DISABLED = "-unavailable-"
PLAYGAME = "Play Game"
CONTROLS = "How to Play"
CREDITS = "Credits"
QUITALL = "Quit"

# This tuple is the main menu that appears on the title screen
#MAINMENU = (ARCADEMODE, VERSUSMODE, QUESTMODE, CONTROLS, CREDITS, QUITALL)
MAINMENU = (GAMEMODE, PLAYERSMODE, PLAYGAME, CONTROLS, CREDITS, QUITALL)
GAMEMODES = (ARCADEMODE, VERSUSMODE, QUESTMODE)

# Unique colors associated with each player number
PLAYERCOLORS = {labyrinthGame.PLAYERS1: graphics.AQUA, labyrinthGame.PLAYERS2: graphics.FUCHSIA}
#                PLAYER3: graphics.MAROON, PLAYER4: graphics.SILVER}

# Clock for regulating game speed
FPSCLOCK = pygame.time.Clock()

def main(args):
    # Initialize pygame
    pygame.init()
    # Initialize graphics, controls
    if graphics.FULLSCREENPARAM in args:
        gfx = graphics.Graphics(DISPLAYWIDTH, DISPLAYHEIGHT, 'Labyrinth', graphics.FULLSCREEN, BGCOLOR)
    else:
        gfx = graphics.Graphics(DISPLAYWIDTH, DISPLAYHEIGHT, 'Labyrinth', graphics.WINDOWED, BGCOLOR)
    controlmap = controls.ControlMap()
    # Tell graphics to load each sprite-sheet, the size of each sprite, and
    # how many steps are in each animation
    allObjStates = labyrinthGame.getAllObjStates()
    if DEBUG in args:
        print(allObjStates)
    for objState in allObjStates:
        name = objState['name']
        gfx.addSprite(name, 'data/' + name + '.png', objState['size'], objState['steps'])
    gfx.addTileSet('data/wallTiles-48x48.png', 'walls', labyrinthObjects.TILESIZE, labyrinthObjects.TILESIZE)
    # Set up and load all sounds
    sound = sounds.Sounds()
    if sounds.NOSOUNDPARAM in args:
        sound.nosound = True
    addAllSounds(sound)
    # Initial screen mode
    screenMode = PLEASEWAIT
    cursorPosition = 0
    prevCursor = cursorPosition
    numPlayers = 1
    # Redraw lets the graphics know something has changed and it's time to
    # update the screen
    redraw = True

    # Program will keep looping until done = True
    done = False

    # MAIN PROGRAM LOOP
    while not done:
        # GET INPUT
        # Update the state of all controllers and get any function keypress
        # A function key lets the user perform optional actions on the program
        functionKey = controlmap.process_events()
        # Escape or Quit will terminate Labyrinth, or act as Reset
        if functionKey == controls.ESCAPEKEY:
            print('Escape key pressed...')
            if screenMode == TITLE:
                done = True
            elif screenMode == PLAYGAME:
                print('Game reset...')
                screenMode = POWEREDBY
                redraw = True
            else:
                print('Escape to Title screen...')
                screenMode = TITLE
                redraw = True
        # Reset returns the mode to the first splash screen
        elif functionKey == controls.RESETKEY:
            screenMode = POWEREDBY
            redraw = True
        # Option is an easter egg that swaps the player sprites, etc.
        elif functionKey == controls.OPTIONKEY:
            if labyrinthGame.PLAYERS1 == labyrinthGame.HEALERS:
                labyrinthGame.PLAYERS1 = labyrinthGame.MAGES
                labyrinthGame.PLAYERS2 = labyrinthGame.HEALERS
            else:
                labyrinthGame.PLAYERS1 = labyrinthGame.HEALERS
                labyrinthGame.PLAYERS2 = labyrinthGame.MAGES
                labyrinthGame.PLAYERNAMES = (labyrinthGame.PLAYERS1,
                                             labyrinthGame.PLAYERS2)
            redraw = True

        # These are the screen modes which act as splash screens, title screen,
        # etc.. They are: Copyright, License, Title, Controls, and Playgame.
        # They cycle through automatically, or by the user pressing any key.
        if screenMode == PLEASEWAIT:
            # This "Please wait..." screen is to allow for mode-switching monitors
            # to fully switch before the splash screens start
            if redraw:
                gfx.blank()
                gfx.update()
                skipTimer = time.time()
                skipTime = 1
                redraw = False
            if controlmap.getAnyButtonAll() or time.time() - skipTimer > skipTime:
                # Move on to License screen
                screenMode = POWEREDBY
                redraw = True
        elif screenMode == POWEREDBY:
            if redraw:
                gfx.blank()
                gfx.showImage('data/Frame.png', 0, 0)
                gfx.showImage('data/Pygame.png', 0, 0)
                gfx.update()
                sound.playSoundEffect('warpin01')
                skipTimer = time.time()
                skipTime = 2
                redraw = False
            if controlmap.getAnyButtonAll() or time.time() - skipTimer > skipTime:
                # Move on to License screen
                screenMode = LICENSE
                redraw = True
        elif screenMode == LICENSE:
            if redraw:
                gfx.blank()
                gfx.showImage('data/Frame.png', 0, 0)
                gfx.showImage('data/License.png', 0, 0)
                gfx.update()
                sound.playSoundEffect('warpout01')
                skipTimer = time.time()
                skipTime = 2
                redraw = False
            if controlmap.getAnyButtonAll() or time.time() - skipTimer > skipTime:
                # Move on to Title screen
                screenMode = TITLE
                redraw = True
        elif screenMode == TITLE:
            if redraw:
                # Draw the Title screen and menu
                showTitleScreen(gfx, cursorPosition, numPlayers)
                # Start the title music and skip timeout
                if not sound.isPlaylistStarted() and not sounds.NOMUSICPARAM in args:
                    sound.startPlaylist()
                    skipTimer = time.time()
                    skipTime = 122
                redraw = False
            # Navigate the menu
            if controlmap.getStick(controls.PLAYER1, controls.DOWN):
                cursorPosition = menuNext(MAINMENU, cursorPosition)
                sound.playSoundEffect('menu00')
                redraw = True
            elif controlmap.getStick(controls.PLAYER1, controls.UP):
                cursorPosition = menuPrev(MAINMENU, cursorPosition)
                sound.playSoundEffect('menu00')
                redraw = True
            elif controlmap.getStick(controls.PLAYER1, controls.LEFT):
#                if MAINMENU[cursorPosition] in (ARCADEMODE, VERSUSMODE) and numPlayers > 1:
                if MAINMENU[cursorPosition] in (PLAYERSMODE) and numPlayers > 1:
                    numPlayers -= 1
                    sound.playSoundEffect('menu00')
                    redraw = True
            elif controlmap.getStick(controls.PLAYER1, controls.RIGHT):
#                if MAINMENU[cursorPosition] in (ARCADEMODE, VERSUSMODE) and numPlayers < 2:
                if MAINMENU[cursorPosition] in (PLAYERSMODE) and numPlayers < 2:
                    numPlayers += 1
                    sound.playSoundEffect('menu00')
                    redraw = True
            # Confirm selection
            selection = confirmMenu(MAINMENU, cursorPosition, controlmap)
            if selection != None:
                sound.playSoundEffect('menu00')
                if selection == PLAYERSMODE:
                    if numPlayers < 2:
                        numPlayers += 1
                    else:
                        numPlayers = 1
                elif selection == PLAYGAME:
                    sound.stopPlaylist()
                    gamestate = labyrinthGame.GameState(DISPLAYWIDTH, DISPLAYHEIGHT, numPlayers)
                    screenMode = PLAYGAME
                elif selection == CONTROLS:
                    screenMode = CONTROLS
                elif selection == CREDITS:
                    scrollPosition = 0
                    screenMode = CREDITS
                elif selection == QUITALL:
                    sound.stopPlaylist()
                    screenMode = QUITALL
                    done = True
                redraw = True
            # Go back to splash screens after a while
            if time.time() - skipTimer > skipTime:
                sound.stopPlaylist()
                sound.advancePlaylist()
                screenMode = POWEREDBY
                redraw = True
        # Start the game
        elif screenMode == PLAYGAME:
            # First, get the current state of controllers to send to game
            # Note: in future this should be a function in controls.py
            sendcontrollers = []
            for p in range(numPlayers):
                stickDirection = None
                buttonPressed = False
                if controlmap.getController(p).stickUp:
                    stickDirection = labyrinthGame.UP
                elif controlmap.getController(p).stickDown:
                    stickDirection = labyrinthGame.DOWN
                if controlmap.getController(p).stickLeft:
                    stickDirection = labyrinthGame.LEFT
                elif controlmap.getController(p).stickRight:
                    stickDirection = labyrinthGame.RIGHT
                if controlmap.getController(p).buttons[controls.BUTTON1] and controlmap.getController(p).button_repeat[controls.BUTTON1]:
                    buttonPressed = True
                else:
                    buttonPressed = False
                controllerDict = {labyrinthGame.STICK: stickDirection, labyrinthGame.BUTTON: buttonPressed}
                sendcontrollers.append(controllerDict)
            if not playGame(gamestate, sendcontrollers, gfx, sound, numPlayers):
                # Return to Copyright screen once game is over
                screenMode = POWEREDBY
        # Show controls and game instructions
        elif screenMode == CONTROLS:
            if redraw:
                monstersImgX = 248
                monstersImgAltX = 360
                monstersImgY = 96
                monstersX = HALFWIDTH
                monstersY = 112
                monstersHeight = 40
                gfx.blank()
                gfx.showImage('data/Frame.png', 0, 0)
                gfx.showListCentered(("Shoot at the undead to destroy them. Be careful; they will shoot back",
                                      "Capture spirit orbs before they are reclaimed by the Labyrinth.",
                                      "Eliminate each wave of monsters. Can you survive all the waves?"),
                                      HALFWIDTH, TOPOFSCREEN, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                gfx.showImage('data/Ghast.png', monstersImgX, monstersImgY)
                gfx.showTextCentered("The Ghast",
                                     monstersX, monstersY, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                gfx.showImage('data/Wight.png', monstersImgAltX, monstersImgY+monstersHeight)
                gfx.showTextCentered("The Wight",
                                     monstersX, monstersY+monstersHeight, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                gfx.showImage('data/Shade.png', monstersImgX, monstersImgY+(monstersHeight*2))
                gfx.showTextCentered("The Shade",
                                     monstersX, monstersY+(monstersHeight*2), graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                gfx.showImage('data/spiritorb.png', monstersImgAltX, monstersImgY+(monstersHeight*3))
                gfx.showTextCentered("Spirit Orb",
                                     monstersX, monstersY+(monstersHeight*3), graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                gfx.showImage('data/Controls.png', 0, 0)
                playersImgX = 104
                playersImgY = 192
                playersX = 128
                playersY = 232
                playersGap = 382
                if labyrinthGame.PLAYERS1 == labyrinthGame.HEALERS:
                    gfx.showImage('data/healers.png', playersImgX, playersImgY)
                    gfx.showTextCentered("The Healers", playersX, playersY, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                    gfx.showImage('data/magi.png', playersImgX+playersGap, playersImgY)
                    gfx.showTextCentered("The Magi", playersX+playersGap, playersY, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                else:
                    gfx.showImage('data/magi.png', playersImgX, playersImgY)
                    gfx.showTextCentered("The Magi", playersX, playersY, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                    gfx.showImage('data/healers.png', playersImgX+playersGap, playersImgY)
                    gfx.showTextCentered("The Healers", playersX+playersGap, playersY, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE, 'data/romulus.ttf')
                gfx.update()
                redraw = False
            if controlmap.getAnyButtonAll():
                # Move on to Title screen
                screenMode = TITLE
                redraw = True
        # Display the game Credits
        elif screenMode == CREDITS:
            scrollSpeed = 8
            if redraw:
                gfx.blank()
                gfx.showImage('data/Frame.png', 0, 0)
                textfile = open('data/CREDITS.txt')
                lines = textfile.read()
                creditsList = lines.splitlines()
                gfx.showListCentered(creditsList, HALFWIDTH, TOPOFSCREEN-scrollPosition, graphics.NORMALFONTCOLOR, graphics.SMALLFONTSIZE)
                gfx.update()
                redraw = False
            if controlmap.getController(controls.PLAYER1).stickDown:
                if scrollPosition < 1008:
                    scrollPosition += scrollSpeed
                    redraw = True
            elif controlmap.getController(controls.PLAYER1).stickUp:
                if scrollPosition > 0:
                    scrollPosition -= scrollSpeed
                    redraw = True
            if controlmap.getAnyButtonAll():
                # Move on to Title screen
                screenMode = TITLE
                redraw = True
        # Limit frames per second
        FPSCLOCK.tick(FPS)
    pygame.quit()

def playGame(gamestate, controlmap, gfx, sound, numPlayers):
    # Update the current game state
    if not gamestate.playGame(controlmap):
        return False
    # Update the game notice and grid
    showGame(gamestate, gfx)
    # Draw the sprites
    spriteObjs = gamestate.getAllObjs()
    gfx.drawAllSprites(spriteObjs)
    # Draw player scoreboards
    showPlayerScores(gamestate, gfx)
    # Play sound effects
    playSoundEffects(gamestate, sound)
    gfx.update()
    return True

def menuNext(menuList, cursorPosition):
    cursor = cursorPosition + 1
    # Returns the cursor, confined within the length of the menu
    return cursor % len(menuList)

def menuPrev(menuList, cursorPosition):
    cursor = cursorPosition - 1
    # Returns the cursor, confined within the length of the menu
    return cursor % len(menuList)    

def confirmMenu(menuList, cursorPosition, controlmap):
    cursor = cursorPosition
    if controlmap.getAnyButtonAll():
        if not cursor < 0 and cursor < len(menuList):
            return menuList[cursor]
    return None

def addAllSounds(sound):
    sound.addSoundEffect('open00', 'data/Inventory_Open_00.ogg')
    sound.addSoundEffect('open01', 'data/Inventory_Open_01.ogg')
    sound.addSoundEffect('menu00', 'data/Menu_Select_00.ogg')
    sound.addSoundEffect('shot01', 'data/Shot_00.ogg')
    sound.addSoundEffect('shot02', 'data/Shot_01.ogg')
    sound.addSoundEffect('hit01', 'data/Hit_00.ogg')
    sound.addSoundEffect('hit02', 'data/Hit_01.ogg')
    sound.addSoundEffect('hit03', 'data/Hit_02.ogg')
    sound.addSoundEffect('death01', 'data/death-01.ogg')
    sound.addSoundEffect('death02', 'data/death-02.ogg')
    sound.addSoundEffect('explosion01', 'data/Explosion_01.ogg')
    sound.addSoundEffect('warpin01', 'data/WarpingIn.ogg')
    sound.addSoundEffect('levelend01', 'data/LevelFinish.ogg')
    sound.addSoundEffect('warpout01', 'data/WarpingOut.ogg')
    sound.addSoundEffect('gameover01', 'data/GameOver.ogg')
    sound.addSoundEffect('collect01', 'data/Collect_Point_00.ogg')
    sound.addSoundList('dirt09', 'data/Footstep_Dirt_0', 10, '.ogg')
    sound.addPlaylist('data/Track_0', 2, '.ogg')

def playSoundEffects(gamestate, sound):
    for player in gamestate.getPlayerObjs():
        if gamestate.status in (labyrinthGame.PLAYING, labyrinthGame.STARTING):
            if player.status == labyrinthObjects.CASTING and player.step == 3:
                sound.playSoundEffect('shot01')
            if player.status == labyrinthObjects.MOVING and player.step in (1, 7):
                sound.playRandomSound('dirt09')
            if player.status == labyrinthObjects.ARRIVING:
                if player.step == 0:
                    sound.playSoundEffect('open01')
                if player.step == 30:
                    sound.playSoundEffect('warpin01')
            if player.status == labyrinthObjects.DYING:
                if player.step == 0:
                    sound.playSoundEffect('death02')
            if player.missile.fired and player.missile.status == labyrinthObjects.DYING:
                if player.missile.step == 0:
                    sound.playSoundEffect('hit01')
                elif player.missile.step == 16:
                    sound.playSoundEffect('death01')
        if gamestate.status == labyrinthGame.ENDING:
            if gamestate.step == labyrinthGame.ENDINGCYCLE:
                sound.playSoundEffect('levelend01')
            if player.status == labyrinthObjects.DEPARTING:
                if player.step == 30:
                    sound.playSoundEffect('warpout01')
    for monster in gamestate.getMonsterObjs():
        if monster.status == labyrinthObjects.DEPARTING and monster.step == 0:
            sound.playSoundEffect('collect01')
        if monster.missile.fired:
            if monster.missile.status == labyrinthObjects.MOVING and monster.missile.step == 0:
                sound.playSoundEffect('shot02')
            elif monster.missile.status == labyrinthObjects.DYING:
                if monster.missile.step == 0:
                    sound.playSoundEffect('hit02')
                    sound.playSoundEffect('hit03')
    if gamestate.step == 0:
        if gamestate.status == labyrinthGame.GAMEOVER:
            sound.playSoundEffect('gameover01')
            
def showGame(gamestate, gfx):
    noticeColor = graphics.WHITE
    noticeX = 320
    noticeY = 80
    noticeWidth = 180
    noticeHeight = 32
    if gamestate.step == 0:
        if gamestate.status == labyrinthGame.STARTING:
            gfx.blank()
            gfx.showImage('data/Frame.png', 0, 0)
            gfx.tileMap('walls', gamestate.getFieldRect(), gamestate.getTileMap(1))
            gfx.update()
            gfx.showTextBoxCentered("Entering Dungeon", noticeColor, BGCOLOR, noticeX, noticeY, noticeWidth, noticeHeight)
        elif gamestate.status == labyrinthGame.PLAYING:
            if gamestate.level < 1:
                gfx.showTextBoxCentered("Level " + str(gamestate.level+1), noticeColor, BGCOLOR, noticeX, noticeY, noticeWidth, noticeHeight)
            else:
                gfx.showTextBoxCentered("Level 1 + " + str(gamestate.level), noticeColor, BGCOLOR, noticeX, noticeY, noticeWidth, noticeHeight)
        elif gamestate.status == labyrinthGame.ENDING:
            gfx.showTextBoxCentered("Dungeon Cleared", noticeColor, BGCOLOR, noticeX, noticeY, noticeWidth, noticeHeight)
        elif gamestate.status == labyrinthGame.GAMEOVER:
            gfx.showTextBoxCentered("Game Over", noticeColor, BGCOLOR, noticeX, noticeY, noticeWidth, noticeHeight)

def showPlayerScores(gamestate, gfx):
    for player in gamestate.getPlayerObjs():
        textLabel = player.name
        livesX = player.outPosition[0]-64
        livesY = player.outPosition[1]+6
        scoreX = player.outPosition[0]+32
        scoreY = player.outPosition[1]+6
        scoreWidth = 80
        scoreHeight = 24
        # Determine players' individual colors
        if player.name in labyrinthGame.PLAYERS1:
            playerColor = PLAYERCOLORS[labyrinthGame.PLAYERS1]
        elif player.name in labyrinthGame.PLAYERS2:
            playerColor = PLAYERCOLORS[labyrinthGame.PLAYERS2]
        # Update lives
        if player.step == 0:
            if player.status == labyrinthGame.ARRIVING:
                gfx.showTextBox(str(player.score), playerColor, BGCOLOR, scoreX, scoreY, scoreWidth, scoreHeight)
                gfx.showTextBox("Left: " + str(player.respawns), graphics.BLACK, BGCOLOR, livesX, livesY, scoreWidth, scoreHeight)
                for i in range(len(player.respawnList)):
                    spriteName = player.respawnList[i].name
                    gfx.showImage('data/' + spriteName + '.png', livesX+(i*16), livesY)
        # Update score
        print(player.missile.status, player.missile.step)
        if player.missile.fired and player.missile.step == 0:
            gfx.showTextBox(str(player.score), playerColor, BGCOLOR, scoreX, scoreY, scoreWidth, scoreHeight)

def showTitleScreen(gfx, cursorPosition, numPlayers):
    menuY = 220
    menuWidth = 200
    menuHeight = 140
    disclaimerY = 392
    gfx.blank()
    gfx.showImage('data/Frame.png', 0, 0)
    gfx.showListCentered(("LABYRINTH",
                          "of the DEAD"),
                          HALFWIDTH, TOPOFSCREEN+(graphics.OVERFONTSIZE/2), graphics.PURPLE, graphics.OVERFONTSIZE, 'data/alagard.ttf')
    menuList = list(MAINMENU)
    # Change the menu items that have a setting
    menuList[0] += " " + ARCADEMODE
    menuList[1] += " " + str(numPlayers) + "P"
    gfx.showMenuList(menuList, graphics.NORMALFONTCOLOR, BGCOLOR, HALFWIDTH, menuY, menuWidth, menuHeight, cursorPosition)
    gfx.showList(("Labyrinth of the Dead  Copyright (C) 2015, 2016  Eric Duhamel",
                  "This program comes with ABSOLUTELY NO WARRANTY.",
                  "This is free software, and you are welcome to redistribute it",
                  "under certain conditions."),
                  graphics.DARKFONTCOLOR, LEFTOFSCREEN, disclaimerY, graphics.SMALLFONTSIZE)
    gfx.update()
    
if __name__ == '__main__':
    main(sys.argv)
