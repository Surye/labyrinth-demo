# Copyright 2015, 2016 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, random, time, copy
random.seed()

import labyrinthObjects
import gameobjects

# General object stats
STICK = 'stick'
BUTTON = 'button'
PLAYERSPEED = 3

STARTCYCLE = 0
RESTARTCYCLE = 1
STARTINGCYCLE = 60
ENDINGCYCLE = 60
DYINGTIME = 2
ENDLEVELDELAY = 4

# Object names
HEALERM = 'healer-m'
HEALERF = 'healer-f'
MAGEM = 'mage-m'
MAGEF = 'mage-f'
PLAYERS = (HEALERM, HEALERF, MAGEM, MAGEF)
HEALERS = (HEALERM, HEALERF)
MAGES = (MAGEM, MAGEF)
PLAYER = 'player'
PLAYERS1 = HEALERS
PLAYERS2 = MAGES
PLAYERNAMES = (PLAYERS1,
               PLAYERS2)
MALE = 0
FEMALE = 1
GHAST = 'ghast'
WIGHT = 'wight'
SHADE = 'shade'
GHOST = 'ghost'
DEATH = 'death'
HOLYMISSILE = 'holy-missile'
MAGICMISSILE = 'magic-missile'
DARKMISSILE = 'dark-missile'
MISSILES = (HOLYMISSILE, MAGICMISSILE, DARKMISSILE)

# Object status
NONE = 'none'
NOTHING = 'nothing'
ARRIVING = 'arriving'
DEPARTING = 'departing'
UP = 'up'
LEFT = 'left'
DOWN = 'down'
RIGHT = 'right'
HIT = 'hit'
DEAD = 'dead'
STANDING = 'standing'
CASTING = 'casting'
WALKING = 'walking'
INVULNERABLE = 'invulnerable'
STARTING = 'starting'
PLAYING = 'playing'
ENDING = 'ending'
GAMEOVER = 'gameover'

class RespawnSprite(labyrinthObjects.Player):
    def __init__(self, playernum, respawnnum):
        self.name = random.choice(PLAYERNAMES[playernum])
        startRect = labyrinthObjects.OUTPOSITIONS[playernum][respawnnum]
        super(RespawnSprite, self).__init__(self.name, startRect)


class NewPlayer(labyrinthObjects.Player):
    def __init__(self, playernum):
        self.type = PLAYER
        self.respawns = labyrinthObjects.PLAYERSPAWNS
        self.respawnList = []
        for i in range(self.respawns):
            self.respawnList.append(RespawnSprite(playernum, i))
        self.name = self.respawnList[0].name
        self.outPosition = labyrinthObjects.OUTPOSITIONS[playernum][i]
        startRect = self.outPosition
        super(NewPlayer, self).__init__(self.name, startRect)


class GameState():
    def __init__(self, windowwidth, windowheight, numPlayers):
        self.wallMaze = labyrinthObjects.Maze1(labyrinthObjects.TILESIZE, labyrinthObjects.SPRITESIZE, windowwidth, windowheight)
        self.WINDOWWIDTH = windowwidth
        self.WINDOWHEIGHT = windowheight
        self.numPlayers = numPlayers
        self.playerObjs = []
        self.monsterObjs = []
        for i in range(4):
            x, y = self.wallMaze.getMonsterSpawnPoint()
            newMonster = labyrinthObjects.getMonster(GHAST, x, y)
            newMonster.centerSelf()
            self.monsterObjs.append(newMonster)
        self.level = 0
        self.status = STARTING
        self.step = -1
        self.endLevelTime = time.time()

    def playGame(self, controlmap):
        # Advance the game cycle by one step
        self.advanceSteps()
        # STARTLEVEL happens when the level has just started
        if self.status == STARTING:
            # Add all the players designated for this game
            if len(self.playerObjs) < 1:
                for p in range(self.numPlayers):
                    i = p
                    while i > len(PLAYERNAMES):
                        i -= 2
                    self.playerObjs.append(NewPlayer(p))
            # Add all the monsters for this level
            if len(self.monsterObjs) < 1:
                for i in range(4):
                    x, y = self.wallMaze.getMonsterSpawnPoint()
                    newMonster = labyrinthObjects.getMonster(GHAST, x, y)
                    newMonster.centerSelf()
                    self.monsterObjs.append(newMonster)
        # ENDLEVEL happens after all monsters are dead and a timer
        elif self.status == ENDING and self.step == ENDINGCYCLE:
            self.level += 1
        # GAMEOVER happens when all players have lost all lives
        elif self.status == GAMEOVER and self.step >= 128:
            if controlmap[0][BUTTON]:
                return False

        # Monsters and Players move during multiple states of the game
        # Monsters move during the start and during a level, and at Game Over
        if self.status in (STARTING, PLAYING, GAMEOVER):
            for monster in self.monsterObjs:
                if monster.status not in (labyrinthObjects.DYING,
                                          labyrinthObjects.ARRIVING,
                                          labyrinthObjects.ORBITING,
                                          labyrinthObjects.DEPARTING,
                                          labyrinthObjects.RESPAWNING):
                    # Decide on a direction and move
                    intentDirection = monster.aiChooseDirection(self.wallMaze)
                    monster.moveOnPath(self.wallMaze, monster.direction, intentDirection)
                    # Fire missile if in range of player
                    if monster.aiDecideFire(self.wallMaze, self.playerObjs):
                        activeMissiles = 0
                        for m in self.monsterObjs:
                            if m.missile.fired:
                                activeMissiles += 1
                        if activeMissiles < 2:
                            monster.fireMissile()
                # Step through all the action cycles and move missile if fired
                monster.advanceSteps()
                monster.stepMissile(self.wallMaze, self.playerObjs)
                # Add or remove monsters when appropriate
                if monster.status == labyrinthObjects.RESPAWNING:
                    x, y = self.wallMaze.getMonsterSpawnPoint()
                    newMonster = labyrinthObjects.getMonster(GHAST, x, y)
                    newMonster.centerSelf()
                    self.monsterObjs.append(newMonster)
                    self.monsterObjs.remove(monster)
                # What to do if the monster is dead
                elif monster.status == labyrinthObjects.DEAD:
                    self.monsterObjs.remove(monster)
                    if len(self.monsterObjs) < 1:
                        self.endLevelTime = time.time()

        # Players can move only after the level has started
        if self.status in (STARTING, PLAYING, ENDING):
            for playerNum, player in enumerate(self.playerObjs):
                # Get the current state of this players controls
                stickDirection = controlmap[playerNum][STICK]
                fireButton = controlmap[playerNum][BUTTON]
                # Control the character based on the input
                if player.status not in (DEAD, labyrinthObjects.DYING, CASTING, ARRIVING, DEPARTING):
                    if stickDirection not in (None, NONE):
                        moveDirection, moveDistance = player.followPath(self.wallMaze, PLAYERSPEED, player.direction, stickDirection, labyrinthObjects.WALLSDICT)
#                        print self.wallMaze.findPaths(player), self.wallMaze.getCellWalls(player.x, player.y).getWalls()
                        player.move(moveDirection, moveDistance)
                        player.direction = moveDirection
                        if player.status != labyrinthObjects.MOVING:
                            player.startStatus(labyrinthObjects.MOVING)
                        player.action = labyrinthObjects.ACTIONS[player.direction]
                        if player.step < labyrinthObjects.PLAYERCYCLES[labyrinthObjects.MOVING]:
                            player.step += 1
                        else:
                            player.step = RESTARTCYCLE
                    # This makes the character stand straight when not walking
                    elif player.status != INVULNERABLE:
                        player.startStatus(labyrinthObjects.STANDING)
                    if fireButton:
                        if not player.missile.fired:
                            player.startStatus(labyrinthObjects.CASTING)
                # Now handle all the action cycles
                player.advanceSteps()
                player.stepMissile(self.wallMaze, self.monsterObjs)
                # Fire missile if done casting
                if player.status == CASTING and player.step == 3:
                    player.fireMissile()
                    if player.name in HEALERS:
                        player.missile.name = HOLYMISSILE
                    elif player.name in MAGES:
                        player.missile.name = MAGICMISSILE
                    player.missile.status = labyrinthObjects.MOVING
                    player.missile.direction = player.direction
                    player.missile.step = 0
                # If player is dead, respawn or endgame
                elif player.status == labyrinthObjects.DYING:
                    if time.time() - player.dyingStartTime > DYINGTIME and player.respawns > 0:
                        player.name = player.respawnList[player.respawns-1].name
                        del player.respawnList[player.respawns-1]
                        player.respawns -= 1
                        player.center = self.wallMaze.getPlayerSpawnPoint()
                        player.startStatus(labyrinthObjects.ARRIVING)
                        gameIsReallyOver = True
                        for p in self.playerObjs:
                            if p.respawns > 0:
                                gameIsReallyOver = False
                        if gameIsReallyOver:
                            self.status = GAMEOVER
                            self.step = -1
                # This brings the player back after departing
                # the previous dungeon
                elif player.status == DEPARTING:
                    player.startStatus(labyrinthObjects.ARRIVING)
                    player.center = self.wallMaze.getPlayerSpawnPoint()

        # Handle collisions
        if self.status in (STARTING, PLAYING, ENDING):
            for monster in self.monsterObjs:
                for player in self.playerObjs:
                    # Upon player colliding with monster
                    if monster.colliderect(player):
                        # Get collected by players if monster is a spirit orb
                        if monster.status == labyrinthObjects.ORBITING:
                            if player.status not in (labyrinthObjects.DYING,
                                                     labyrinthObjects.INVULNERABLE,
                                                     labyrinthObjects.ARRIVING,
                                                     labyrinthObjects.DEPARTING):
                                monster.startStatus(labyrinthObjects.DEPARTING)
                    # Upon missile colliding with monster
                    if player.missile.fired and player.missile.status not in (labyrinthObjects.DYING, labyrinthObjects.DEPARTING):
                        if monster.status not in (labyrinthObjects.DYING,
                                                  labyrinthObjects.ORBITING,
                                                  labyrinthObjects.DEPARTING):
                            if player.missile.colliderect(monster):
                                monster.status = labyrinthObjects.DYING
                                monster.step = labyrinthObjects.STARTCYCLE
                                if monster.missile.status != labyrinthObjects.DEPARTING:
                                    monster.missile.status = labyrinthObjects.DEPARTING
                                    monster.missile.step = labyrinthObjects.STARTCYCLE
                                player.score += monster.points
                                player.missile.width = 32
                                player.missile.height = 32
                                player.missile.center = monster.center
                                player.missile.status = labyrinthObjects.DYING
                                player.missile.step = labyrinthObjects.STARTCYCLE
                    # Upon missile colliding with player
                    if monster.missile.fired and monster.missile.status not in (labyrinthObjects.DYING, labyrinthObjects.DEPARTING):
                        if player.status not in (labyrinthObjects.DYING, labyrinthObjects.INVULNERABLE,
                                                 labyrinthObjects.ARRIVING, labyrinthObjects.DEPARTING):
                            # If all conditions met, resolve the collision
                            if monster.missile.colliderect(player):
                                player.status = labyrinthObjects.DYING
                                player.step = 0
                                monster.missile.width = 32
                                monster.missile.height = 32
                                monster.missile.center = player.center
                                monster.missile.status = labyrinthObjects.DYING
                                monster.missile.step = 0
                                player.dyingStartTime = time.time()

        # Done with this game cycle
        # Return True and wait to start the next cycle
        return True

    # advanceSteps() iterates the status of the game, looping through cycles
    def advanceSteps(self):        
        if self.status == STARTING:
            if self.step < STARTINGCYCLE:
                self.step += 1
            else:
                self.status = PLAYING
                self.step = -1
        elif self.status == ENDING:
            if self.step < ENDINGCYCLE:
                self.step += 1
            else:
                self.status = STARTING
                self.step = -1
        elif self.status == GAMEOVER:
            if self.step < 128:
                self.step += 1
        elif self.status == PLAYING:
            if len(self.monsterObjs) < 1:
                self.status = ENDING
                self.step = STARTCYCLE
            elif self.step < 256:
                self.step += 1

    def getFieldRect(self):
        return self.wallMaze

    def getTileMap(self, mapIndex):
        if mapIndex == 1:
            return self.wallMaze.wallmap
        elif mapIndex == 2:
            return self.wallMaze.othwallmap

    def getPlayerObjs(self):
        return self.playerObjs

    def getMonsterObjs(self):
        return self.monsterObjs

    def getAllObjs(self):
        spriteList = []
        i = 1
        for sprite in self.playerObjs:
            spriteDict = {}
            spriteDict['name'] = sprite.name + '-' + sprite.status
            spriteDict['x'] = sprite.x
            spriteDict['y'] = sprite.y
            spriteDict['step'] = sprite.step
            if sprite.status in (STANDING, labyrinthObjects.MOVING, CASTING):
                spriteDict['direction'] = labyrinthObjects.ACTIONS[sprite.direction]
            else:
                spriteDict['direction'] = 0
            if self.wallMaze.contains(sprite):
                spriteList.append(spriteDict)
            if sprite.missile.fired:
                spriteDict = {}
                spriteDict['name'] = sprite.missile.name + '-' + sprite.missile.status
                spriteDict['x'] = sprite.missile.x
                spriteDict['y'] = sprite.missile.y
                spriteDict['step'] = sprite.missile.step
                if sprite.missile.status == labyrinthObjects.DYING:
                    spriteDict['direction'] = 0
                else:
                    spriteDict['direction'] = labyrinthObjects.ACTIONS[sprite.missile.direction]
                spriteList.append(spriteDict)
            i += 1
        i = 1
        for sprite in self.monsterObjs:
            spriteDict = {}
            spriteDict['name'] = sprite.type + '-' + sprite.status
            spriteDict['x'] = sprite.x
            spriteDict['y'] = sprite.y
            spriteDict['step'] = sprite.step
            spriteDict['direction'] = sprite.action
            spriteList.append(spriteDict)
            if sprite.missile.fired:
                spriteDict = {}
                spriteDict['name'] = sprite.missile.name + '-' + sprite.missile.status
                spriteDict['x'] = sprite.missile.x
                spriteDict['y'] = sprite.missile.y
                spriteDict['step'] = sprite.missile.step
                if sprite.missile.status == labyrinthObjects.DYING:
                    spriteDict['direction'] = 0
                else:
                    spriteDict['direction'] = labyrinthObjects.ACTIONS[sprite.missile.direction]
                spriteList.append(spriteDict)
            i += 1
        return spriteList


# This function returns the names of all the actions and the number of
# game cycles in each, for Main to load graphics.
def getAllObjStates():
    statesList = []
    for obj in labyrinthObjects.MONSTERS:
        for state in labyrinthObjects.MONSTERCYCLES:
            monsterState = {'name': obj + '-' + state,
                            'size': labyrinthObjects.MONSTERSIZES2[state],
                            'steps': labyrinthObjects.MONSTERCYCLES[state]}
            statesList.append(monsterState)
    for obj in labyrinthObjects.PLAYERS:
        for state in labyrinthObjects.PLAYERCYCLES:
            playerState = {'name': obj + '-' + state,
                           'size': labyrinthObjects.PLAYERSIZES2[state],
                           'steps': labyrinthObjects.PLAYERCYCLES[state]}
            statesList.append(playerState)
    for obj in labyrinthObjects.MISSILES:
        for state in labyrinthObjects.MISSILECYCLES:
            missileState = {'name': obj + '-' + state,
                            'size': labyrinthObjects.MISSILESIZES2[state],
                            'steps': labyrinthObjects.MISSILECYCLES[state]}
            statesList.append(missileState)
    return statesList
